package com.compassites.randomproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private View bExpand;
    private View bShrink;
    private boolean isExpanded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        bShrink = findViewById(R.id.b_shrink);
        bShrink.setOnClickListener(this);
        bExpand = findViewById(R.id.b_expand);
        bExpand.setOnClickListener(this);
        addItemsToView(false);
    }

    private void addItemsToView(boolean isScrollable) {
        LinearLayout llItemHolder = (LinearLayout) findViewById(R.id.ll_item_holder);
        llItemHolder.removeAllViews();

        int itemCount = isScrollable ? 5 : 1;
        for(int i = 0; i < itemCount; i++ ) {
            getLayoutInflater().inflate(R.layout.item_text, llItemHolder, true);
        }

        isExpanded = isScrollable;
        toggleButtonState();
    }

    private void toggleButtonState() {
        bExpand.setEnabled(!isExpanded);
        bShrink.setEnabled(isExpanded);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b_expand:
                addItemsToView(true);
                break;

            case R.id.b_shrink:
                addItemsToView(false);
                break;
        }
    }
}
